var btn = document.getElementById("sent");
btn.addEventListener("click", validateForm);

function validateForm() {
    let result = true;
    var emri = document.forma.emri.value;

    let errorEmri = document.querySelector(".emri");
    if (emri.trim().length == 0) {
        alert("Please fill in the 'Name' box.");
        document.forma.emri.focus();
        errorEmri.innerHTML = "Plotesojeni fushen!"
        errorEmri.className = "error emri active";
        return result = false;
    }else{
    	errorEmri.innerHTML = "";
    	errorEmri.className = "error emri";
    }

    let errorEmail = document.querySelector(".email");
    var email = document.forma.email.value;
    if (email.trim().length == 0) {
        alert("Please fill in the 'Email' box.");
        document.forma.email.focus();
        errorEmail.innerHTML = "Plotesojeni fushen!"
        errorEmail.className = "error email active";
        return result = false;
    }else{
    	errorEmail.innerHTML = "";
    	errorEmail.className = "error email";
    }
    var message = document.forma.message.value;

    let errorMessage = document.querySelector(".message");
    if (message.trim().length == 0) {
        alert("Please fill in the 'message' box.");
        document.forma.message.focus();
        errorMessage.innerHTML = "Plotesojeni fushen!"
        errorMessage.className = "error message active";
        return result = false;
    }else{
    	errorMessage.innerHTML = "";
    	errorMessage.className = "error message";
    }

   return result;
}
